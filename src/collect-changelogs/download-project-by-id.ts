import fs from 'node:fs';
import { posix as path } from 'node:path';
import { format } from 'node:util';
import addAuthToGitHttpUrl from '@common/add-auth-to-git-http-url';
import { KNOWN_ERROR_EXIT_CODE } from '@common/constants';
import HandledError from '@common/errors/handled-error';
import { createDefaultPortalExec } from '@common/portal-exec';
import dateToDayPath from './date-to-day-path';
import daysInRange from './days-in-range';
import getMergeRequestTitle from './gitlab/get-merge-request-title';
import getProjectById from './gitlab/get-project-by-id';
import isFilenameAValidChangelogFilename from './is-filename-a-valid-changelog-filename';
import TemporaryDirectory from './temporary-directory';


interface ChangelogFile {
  readonly filePathFromChangelogsDirectory: string;
  readonly changelogMergeRequestIid: number;
  readonly absolutePath: string;
}

export interface CopiedChangelogFile {
  readonly mergeRequestIid: number;
  readonly pathFromProjectChangelogsPath: string;
  readonly absolutePath: string;
}

export interface DownloadedProjectChangelog {
  readonly file: {
    readonly content: string;
  };
  readonly mergeRequest: {
    readonly iid: number;
    readonly title: string;
  };
}

export interface DownloadedProject {
  readonly id: number;
  readonly defaultBranch: string;
  readonly sshUrlToRepo: string;
  readonly httpUrlToRepo: string;
  readonly webUrl: string;
  readonly nameWithNamespace: string;
  changelogsInDates(
    startDate: Date,
    endDate: Date,
  ): AsyncGenerator<DownloadedProjectChangelog, void>;
}


function extractMergeRequestIidFromChangelogFileName(
  filename: string,
): number {
  const match = /^merge-request-\d+\.md$/.exec(filename);
  const filenameSplits = filename.slice(0, -'.md'.length).split('-');
  if (null === match || filenameSplits.length !== 3) {
    throw new HandledError(
      format('expected changelog filename match the pattern. found %s', filename),
      KNOWN_ERROR_EXIT_CODE,
    );
  }

  return Number.parseInt(filenameSplits[2] as string, 10);
}


function getProjectChangelogFilesInDateRange(
  startDate: Date,
  endDate: Date,
  projectPath: string,
): readonly ChangelogFile[] {
  const changelogFiles: ChangelogFile[] = [];

  for (const day of daysInRange(startDate, endDate)) {
    const dateDirectoryPath = dateToDayPath(day);
    const changelogsDirectoryPath = path.join(
      projectPath,
      'changelogs',
    );
    const dayChangelogsDirectoryPath = path.join(
      changelogsDirectoryPath,
      dateDirectoryPath,
    );
    if (
      // eslint-disable-next-line security/detect-non-literal-fs-filename
      !fs.existsSync(dayChangelogsDirectoryPath)
      // eslint-disable-next-line security/detect-non-literal-fs-filename
      || !fs.statSync(dayChangelogsDirectoryPath).isDirectory()
    ) {
      // eslint-disable-next-line no-continue
      continue;
    }

    // eslint-disable-next-line security/detect-non-literal-fs-filename
    const directoryEntries = fs.readdirSync(
      dayChangelogsDirectoryPath,
      { withFileTypes: true, encoding: 'utf8' },
    );

    for (const entry of directoryEntries) {
      if (
        entry.isFile()
        && !entry.isSymbolicLink()
        && isFilenameAValidChangelogFilename(entry.name)
      ) {
        const mrIid = extractMergeRequestIidFromChangelogFileName(entry.name);
        changelogFiles.push({
          changelogMergeRequestIid: mrIid,
          filePathFromChangelogsDirectory: path.join(
            dateDirectoryPath,
            entry.name,
          ),
          absolutePath: path.join(dayChangelogsDirectoryPath, entry.name),
        });
      }
    }
  }

  return changelogFiles;
}

export default async function downloadProjectById(
  gitlabApiBaseUrl: string,
  authToken: string,
  projectId: number,
): Promise<DownloadedProject> {
  const downloadedProjectDirectory = new TemporaryDirectory();
  const project = await getProjectById({
    authToken,
    projectId,
    gitlabApiBaseUrl,
  });
  const gitRemoteUrl = addAuthToGitHttpUrl({
    authToken,
    gitUsername: EMBED_GIT_USER_NAME,
    repositoryHttpUrl: project.httpUrlToRepo,
  });
  console.debug(
    format('> creating exec in "%s"', downloadedProjectDirectory.path),
  );
  const execInProjectDirectory = createDefaultPortalExec(
    downloadedProjectDirectory.path,
  );
  execInProjectDirectory(
    'git',
    ['init'],
    [],
  );
  execInProjectDirectory(
    'git',
    [
      'remote',
      'add',
      'origin',
      '%s',
    ],
    [gitRemoteUrl],
  );
  execInProjectDirectory(
    'git',
    [
      'fetch',
      '--depth',
      '1',
    ],
    [],
  );
  execInProjectDirectory(
    'git',
    [
      'pull',
      'origin',
      project.defaultBranch,
    ],
    [],
  );

  return {
    id: projectId,
    defaultBranch: project.defaultBranch,
    sshUrlToRepo: project.sshUrlToRepo,
    httpUrlToRepo: project.httpUrlToRepo,
    webUrl: project.webUrl,
    nameWithNamespace: project.nameWithNamespace,
    async * changelogsInDates(startDate, endDate) {
      const changelogFiles = getProjectChangelogFilesInDateRange(
        startDate,
        endDate,
        downloadedProjectDirectory.path,
      );

      for (const changelogFile of changelogFiles) {
        // eslint-disable-next-line no-await-in-loop
        const mrTitle = await getMergeRequestTitle({
          apiBaseUrl: gitlabApiBaseUrl,
          authToken,
          mergeRequestIid: changelogFile.changelogMergeRequestIid,
          projectId,
        });
        yield {
          file: {
            // eslint-disable-next-line security/detect-non-literal-fs-filename
            content: fs.readFileSync(changelogFile.absolutePath, { encoding: 'utf8' }),
          },
          mergeRequest: {
            iid: changelogFile.changelogMergeRequestIid,
            title: mrTitle,
          },
        };
      }
    },
  };
}
