// eslint-disable-next-line import/no-extraneous-dependencies
import test from 'ava';
import { UNKNOWN_ERROR_EXIT_CODE } from './constants';
import NotEnoughReplacements from './errors/not-enough-replacements';
import NotEnoughSpecifiers from './errors/not-enough-specifiers';
import {
  createDefaultRedactor,
  createDefaultExecCommandArgumentsFormatter,
} from './exec';


test('default redactor should mask command arguments with one specifier', t => {
  const redactor = createDefaultRedactor();

  const redactedMessage = redactor(
    'ssh',
    ['please', 'mask', '%s', 'now'],
    ['me'],
  );

  t.is(redactedMessage, '+ ssh \'please\' \'mask\' \'**\' \'now\'');
});

test('default redactor should mask command arguments with 4 specifiers', t => {
  const redactor = createDefaultRedactor();

  const redactedMessage = redactor(
    'ssh',
    ['Lorem', 'Ipsum', 'is', 'simply', 'dummy', '%s', 'of', 'the', '%s', 'and', '%s', '%s'],
    ['text', 'printing', 'typesetting', 'industry'],
  );

  t.is(
    redactedMessage,
    '+ ssh \'Lorem\' \'Ipsum\' \'is\' \'simply\' \'dummy\' \'****\' \'of\' \'the\' \'********\' \'and\' \'***********\' \'********\'',
  );
});

test('default redactor should mask command arguments with multiple specifiers in one argument', t => {
  const redactor = createDefaultRedactor();

  const redactedMessage = redactor(
    'ssh',
    ['Lorem', 'Ipsum', 'is', 'simply dummy %s of the %s and %s', '%s'],
    ['text', 'printing', 'typesetting', 'industry'],
  );

  t.is(
    redactedMessage,
    '+ ssh \'Lorem\' \'Ipsum\' \'is\' \'simply dummy **** of the ******** and ***********\' \'********\'',
  );
});

test('default redactor should throw error when there are no specifiers but there are replacements', t => {
  const redactor = createDefaultRedactor();

  t.plan(1);

  try {
    redactor(
      'ssh',
      ['please', 'mask', 'it', 'now'],
      ['me', 'then'],
    );
  } catch (error: unknown) {
    if (error instanceof NotEnoughSpecifiers) {
      t.deepEqual(error.replacements, ['me', 'then']);
    }
  }
});

test('default redactor should throw error when there are specifiers but there are no replacements', t => {
  const redactor = createDefaultRedactor();

  t.plan(1);

  try {
    redactor(
      'ssh',
      ['please', 'mask', '%s', '%s'],
      [],
    );
  } catch (error: unknown) {
    if (error instanceof NotEnoughReplacements) {
      t.deepEqual(error.exceedingSpecifiers, UNKNOWN_ERROR_EXIT_CODE);
    }
  }
});

test('default redactor should throw error when there are not enough specifiers comparing the replacements', t => {
  const redactor = createDefaultRedactor();

  t.plan(1);

  try {
    redactor(
      'ssh',
      ['please', 'mask', '%s', 'now', 'not', 'later'],
      ['lorem', 'eynak', 'live'],
    );
  } catch (error: unknown) {
    if (error instanceof NotEnoughSpecifiers) {
      t.deepEqual(error.replacements, ['eynak', 'live']);
    }
  }
});

test('default redactor should throw error when there are not enough replacements comparing the specifiers', t => {
  const redactor = createDefaultRedactor();

  t.plan(1);

  try {
    redactor(
      'ssh',
      ['please', '%s', 'it', '%s'],
      ['me'],
    );
  } catch (error: unknown) {
    if (error instanceof NotEnoughReplacements) {
      t.deepEqual(error.exceedingSpecifiers, 1);
    }
  }
});

test('default exec command arguments formatter should replace replacements correctly', t => {
  const formatter = createDefaultExecCommandArgumentsFormatter();

  const result = formatter(
    ['please', 'replace', '%s', 'now'],
    ['this'],
  );

  t.deepEqual(result, ['please', 'replace', 'this', 'now']);
});

test('default exec command arguments formatter should replace replacements among an argument correctly', t => {
  const formatter = createDefaultExecCommandArgumentsFormatter();

  t.deepEqual(
    formatter(
      ['please', 'replace', 'parse %s option', 'now'],
      ['this'],
    ),
    ['please', 'replace', 'parse this option', 'now'],
  );

  t.deepEqual(
    formatter(
      ['please', 'replace', '%s option', 'now'],
      ['this'],
    ),
    ['please', 'replace', 'this option', 'now'],
  );

  t.deepEqual(
    formatter(
      ['please', 'replace', '%sslow', 'now'],
      ['this'],
    ),
    ['please', 'replace', 'thisslow', 'now'],
  );

  t.deepEqual(
    formatter(
      ['please', 'replace', 'some%sslow', 'now'],
      ['this'],
    ),
    ['please', 'replace', 'somethisslow', 'now'],
  );

  t.deepEqual(
    formatter(
      ['please', 'replace', 'some%s', 'now'],
      ['this'],
    ),
    ['please', 'replace', 'somethis', 'now'],
  );
});
