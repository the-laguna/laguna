// eslint-disable-next-line import/no-extraneous-dependencies
import test from 'ava';
import { formattedTime } from './logger';


test('formattedTime should return correctly formatted passed date object', t => {
  const date = new Date('2021-09-27T11:02:52.428Z');
  const result = formattedTime(date);

  t.is(result, '[2021-09-27 11:02:52.428]');
});
