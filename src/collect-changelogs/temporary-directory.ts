import { nanoid } from 'nanoid';
import tmp from 'tmp';
import type { DirResult } from 'tmp';


export default class TemporaryDirectory {
  private readonly directory: DirResult;

  public readonly path: string;

  public constructor() {
    tmp.setGracefulCleanup();
    const DIRECTORY_NAME_POSTFIX_LENGTH = 64;
    this.directory = tmp.dirSync({
      prefix: 'laguna_collector_git_clone',
      postfix: nanoid(DIRECTORY_NAME_POSTFIX_LENGTH),
      tries: 10,
      unsafeCleanup: true,
    });
    this.path = this.directory.name;
  }

  public clean(): void {
    this.directory.removeCallback();
  }
}
