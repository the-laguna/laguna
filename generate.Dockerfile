#syntax=docker/dockerfile-upstream:master
FROM node:16-alpine AS build

RUN apk update && apk upgrade && apk add make && npm -g i npm@8 && npm -g i pnpm@6

WORKDIR /app

COPY . /app/

RUN pnpm install

RUN --mount=type=secret,required=true,id=dotenv,target=/app/.env,mode=0400 \
  set -a; . /app/.env; set +a; make build-generate

FROM node:16-alpine

COPY alpine.repositories /etc/apk/repositories

RUN apk update && apk upgrade && apk --no-cache add git expect gnupg

RUN \
  --mount=type=secret,required=true,id=gpgkeypath,target=/build/gpgkeypath,mode=0400 \
  --mount=type=secret,required=true,id=gpgkey,target=/build/private.key,mode=0400 \
  GPG_KEY_PATH=$(cat /build/gpgkeypath) && cp /build/private.key "$GPG_KEY_PATH"

COPY --from=build /app/build/bin/laguna-generate-dot-changelog /usr/local/bin/laguna-generate-dot-changelog

RUN ln -sf /usr/local/bin/laguna-generate-dot-changelog /usr/local/bin/docker-entrypoint.sh

ENTRYPOINT [ "/usr/local/bin/docker-entrypoint.sh" ]