export default function isFilenameAValidDotChangelogFilename(
  filename: string,
): boolean {
  return /^merge-request-\d+\.md$/.test(filename);
}
