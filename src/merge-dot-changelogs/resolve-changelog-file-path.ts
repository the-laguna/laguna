import { posix as path } from 'node:path';
import { format } from 'node:util';
import dayjs from 'dayjs';
import { KNOWN_ERROR_EXIT_CODE } from '@common/constants';
import HandledError from '@common/errors/handled-error';


function mergeRequestMergedDateToPath(mergeDate: Date): string {
  const date = dayjs(mergeDate);

  return [
    date.format('YYYY'),
    date.format('MM'),
    date.format('DD'),
  ].join(path.sep);
}

interface ChangelogMergeRequest {
  readonly iid: string;
  readonly mergedDate: Date;
}

export default function resolveChangelogFilePath(
  changelogsDirectory: string,
  changelogMergeRequest: ChangelogMergeRequest,
): string {
  try {
    return path.join(
      changelogsDirectory,
      mergeRequestMergedDateToPath(changelogMergeRequest.mergedDate),
      `merge-request-${changelogMergeRequest.iid}.md`,
    );
  } catch {
    throw new HandledError(
      format('unable to resolve changelog file path'),
      KNOWN_ERROR_EXIT_CODE,
    );
  }
}
