import color from 'chalk';
import execa from 'execa';
import HandledError from './errors/handled-error';
import type {
  CreateExecConfig,
  Exec,
  ExecResult,
} from './exec';
import {
  createDefaultExecCommandArgumentsFormatter,
  createDefaultRedactor,
  resolveExecaErrorExitCode,
  resolveExecaErrorMessage,
} from './exec';


interface CreatePortalExecConfig extends CreateExecConfig {
  readonly cwd: string;
}

export function createPortalExec(
  config: CreatePortalExecConfig,
): Exec {
  return function (
    command,
    commandArguments,
    replacements,
  ): ExecResult {
    const redactedMessage = config.redactor(
      command,
      commandArguments,
      replacements,
    );

    console.debug(color.blue(redactedMessage));
    try {
      const out = execa.sync(
        command,
        config.execCommandArgumentsFormatter(
          commandArguments,
          replacements,
        ),
        {
          cwd: config.cwd,
        },
      );


      return {
        exitCode: out.exitCode,
        stderr: out.stderr,
        stdout: out.stdout,
        failed: out.failed,
        killed: out.killed,
        timedOut: out.timedOut,
      };
    } catch (error: unknown) {
      const message = resolveExecaErrorMessage(error);
      const exitCode = resolveExecaErrorExitCode(error);
      throw new HandledError(message, exitCode);
    }
  };
}

export function createDefaultPortalExec(
  cwd: string,
): Exec {
  return createPortalExec({
    cwd,
    execCommandArgumentsFormatter: createDefaultExecCommandArgumentsFormatter(),
    redactor: createDefaultRedactor(),
  });
}
