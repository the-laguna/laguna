import { format } from 'node:util';
import { KNOWN_ERROR_EXIT_CODE } from '@common/constants';
import HandledError from '@common/errors/handled-error';
import NotFoundError from '@common/errors/not-found-error';
import UnauthorizedError from '@common/errors/unauthorized-error';
import UnknownGitlabError from '@common/errors/unknown-gitlab-error';
import type { GitLabGetMergeRequestRequest } from '@common/gitlab/get-merge-request';
import getMergeRequest from '@common/gitlab/get-merge-request';


export default async function getMergeRequestTitle(
  context: GitLabGetMergeRequestRequest,
): Promise<string> {
  try {
    const mr = await getMergeRequest(context);
    return mr.title;
  } catch (error: unknown) {
    if (error instanceof UnauthorizedError) {
      throw new HandledError(
        'received unauthorized error from GitLab.',
        KNOWN_ERROR_EXIT_CODE,
      );
    }

    if (error instanceof NotFoundError) {
      throw new HandledError(
        format('received not found error from GitLab (%s).', error.message),
        KNOWN_ERROR_EXIT_CODE,
      );
    }

    if (error instanceof UnknownGitlabError) {
      throw new HandledError(
        format('received unknown error from GitLab (%s).', error.message),
        KNOWN_ERROR_EXIT_CODE,
      );
    }

    throw new HandledError(
      format('unhandled error occurred while requesting for merge request title: %s', error),
      KNOWN_ERROR_EXIT_CODE,
    );
  }
}
