import dayjs from 'dayjs';


export interface Configurations {
  readonly jobLastRunDate: null | Date;
  readonly userSetStart: 'week' | 'month' | 'unset';
}

export default function determineStartDate(
  now: Date,
  r: Configurations,
): Date {
  switch (r.userSetStart) {
    case 'month':
      return dayjs(now).subtract(1, 'month').toDate();
    case 'week':
      return dayjs(now).subtract(1, 'week').toDate();
    default:
      break;
  }

  if (null !== r.jobLastRunDate) {
    return r.jobLastRunDate;
  }

  return dayjs(now).subtract(1, 'month').toDate();
}
