import { ensureExists, ensureIsNotEmpty, ensureIsString } from './environment-variables';


export default function ensureCommitReferenceNameEnvironmentVariable(): void {
  ensureExists('CI_COMMIT_REF_NAME');

  const value = process.env['CI_COMMIT_REF_NAME'] ?? '';

  ensureIsNotEmpty('CI_COMMIT_REF_NAME', value);
  ensureIsString('CI_COMMIT_REF_NAME', value);
}
