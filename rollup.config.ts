/* eslint-disable node/no-unpublished-import */
import { EOL } from 'node:os';
import { posix as path } from 'node:path';
import { fileURLToPath } from 'node:url';
import alias from '@rollup/plugin-alias';
import commonjs from '@rollup/plugin-commonjs';
import { nodeResolve } from '@rollup/plugin-node-resolve';
import replace from '@rollup/plugin-replace';
import strip from '@rollup/plugin-strip';
import color from 'chalk';
import type { RollupOptions } from 'rollup';
import { terser } from 'rollup-plugin-terser';
import createCliEntry from './build-plugins/create-cli-entry';
import executable from './build-plugins/executable';


function environmentVariableSet(name: string): boolean {
  return Object.keys(process.env).includes(name);
}

export function ensureEnvironmentVariablesSet(
  variableKeys: readonly string[],
): void | never {
  const unsetVariables = variableKeys
    .map<[string, boolean]>(key => [key, environmentVariableSet(key)])
    .filter(pair => !pair[1])
    .map(pair => pair[0]);

  if (0 === unsetVariables.length) {
    return;
  }

  console.error(
    color.red(
      [
        'the following required environment variables are not set:',
        Array.from(unsetVariables, key => color.bgRed.black(` ${key} `)).join(EOL),
      ].join(EOL),
    ),
  );

  // eslint-disable-next-line no-process-exit, unicorn/no-process-exit
  process.exit(1);
}

type EnvironmentVariableReplacer = () => Record<string, string>;

const dirname = path.dirname(fileURLToPath(import.meta.url));

export function createSharedConfigs(
  environmentVariableReplacer: EnvironmentVariableReplacer,
): RollupOptions {
  return {
    strictDeprecations: true,
    external: [
      'node:assert/strict',
      'node:fs',
      'node:path',
      'node:stream',
      'node:util',
      'node:os',
      'node:url',
    ],
    plugins: [
      nodeResolve({
        browser: false,
        preferBuiltins: true,
      }),
      alias({
        entries: [
          { find: '@common', replacement: path.resolve(dirname, './src/common') },
        ],
      }),
      commonjs({
        ignoreTryCatch: [],
      }),
      strip({
        debugger: true,
        functions: [],
      }),
      replace({
        preventAssignment: false,
        values: {
          ...environmentVariableReplacer(),
        },
      }),
      terser({
        format: {
          comments: false,
        },
      }),
      createCliEntry(),
      executable(),
    ],
    treeshake: {
      moduleSideEffects: false,
      propertyReadSideEffects: false,
      tryCatchDeoptimization: false,
    },
  };
}
