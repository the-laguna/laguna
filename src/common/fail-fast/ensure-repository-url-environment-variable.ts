import assert, { AssertionError } from 'node:assert/strict';
import { format } from 'node:util';
import FailFastError from '@common/errors/fail-fast-error';
import { ensureExists, ensureIsString, ensureValidUrl } from './environment-variables';


function ensureHasValidUsername(url: URL): void {
  try {
    assert.strictEqual(url.username, 'gitlab-ci-token');
  } catch (error: unknown) {
    if (error instanceof AssertionError) {
      throw new FailFastError('"CI_REPOSITORY_URL" contains unexpected "username". It is probably an error caused by your customizations, not GitLab. Make sure you did not override this variable accidentally.');
    }

    throw new FailFastError(
      format('unknown error occurred while validating "CI_REPOSITORY_URL" environment variable for being a valid url: %s', error),
    );
  }
}

function ensureHasPassword(url: URL): void {
  try {
    assert.strictEqual(url.password.length > 0, true);
  } catch (error: unknown) {
    if (error instanceof AssertionError) {
      throw new FailFastError('"CI_REPOSITORY_URL" must contain "password". It is probably an error caused by your customizations, not GitLab. Make sure you did not override this variable accidentally.');
    }

    throw new FailFastError(
      format('unknown error occurred while validating "CI_REPOSITORY_URL" environment variable for being a valid url: %s', error),
    );
  }
}

function ensureIsAGitUrl(url: URL): void {
  try {
    assert.strictEqual(url.pathname.endsWith('.git'), true);
  } catch (error: unknown) {
    if (error instanceof AssertionError) {
      throw new FailFastError('"CI_REPOSITORY_URL" must end with ".git" so that it can be used for git operations. It is probably an error caused by your customizations, not GitLab. Make sure you did not override this variable accidentally.');
    }

    throw new FailFastError(
      format('unknown error occurred while validating "CI_REPOSITORY_URL" environment variable for being a valid v4 GitLab API url: %s', error),
    );
  }
}

export default function ensureRepositoryUrlEnvironmentVariable(): void {
  ensureExists('CI_REPOSITORY_URL');

  const value = process.env['CI_REPOSITORY_URL'] ?? '';

  ensureIsString('CI_REPOSITORY_URL', value);
  ensureValidUrl('CI_REPOSITORY_URL', value);

  const url = new URL(value);

  ensureHasValidUsername(url);
  ensureHasPassword(url);
  ensureIsAGitUrl(url);
}
