import { posix as path } from 'node:path';
import { format } from 'node:util';
import { KNOWN_ERROR_EXIT_CODE } from './constants';
import HandledError from './errors/handled-error';


export default function resolveDotChangelogFilePath(
  dotChangelogsDirectoryPath: string,
  dotChangelogFilename: string,
): string {
  try {
    return path.join(
      dotChangelogsDirectoryPath,
      dotChangelogFilename,
    );
  } catch {
    throw new HandledError(
      format('unable to resolve temporary changelog file path'),
      KNOWN_ERROR_EXIT_CODE,
    );
  }
}
