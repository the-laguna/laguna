// eslint-disable-next-line import/no-extraneous-dependencies
import test from 'ava';
import dateToDayPath from './date-to-day-path';


test('it should create directory path with correct number paddings', t => {
  const date = new Date('September 27, 2021 11:58:13');
  t.is(dateToDayPath(date), '2021/09/27');
});
