import fs from 'node:fs';
import { format } from 'node:util';
import { KNOWN_ERROR_EXIT_CODE } from '@common/constants';
import HandledError from '@common/errors/handled-error';
import isFilenameAValidDotChangelogFilename from '@common/is-filename-a-valid-dot-changelog-filename';


function extractMergeRequestIdFromChangelogFilename(filename: string): string {
  const match = /^merge-request-\d+\.md$/.exec(filename);
  const filenameSplits = filename.slice(0, -'.md'.length).split('-');
  if (null === match || filenameSplits.length !== 3) {
    throw new HandledError(
      format('expected changelog filename match the pattern. found %s', filename),
      KNOWN_ERROR_EXIT_CODE,
    );
  }

  return filenameSplits[2] as string;
}

export default function getDotChangelogMergeRequestIds(
  dotChangelogsDirectoryPath: string,
): readonly string[] {
  // eslint-disable-next-line security/detect-non-literal-fs-filename
  return fs.readdirSync(
    dotChangelogsDirectoryPath,
    { encoding: 'utf8', withFileTypes: true },
  )
    .filter(f => f.isFile() && isFilenameAValidDotChangelogFilename(f.name))
    .map(f => extractMergeRequestIdFromChangelogFilename(f.name));
}
