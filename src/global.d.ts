declare const EMBED_PACKAGE_NAME: string;
declare const EMBED_PACKAGE_DESCRIPTION: string;
declare const EMBED_PACKAGE_VERSION: string;

declare const EMBED_GPG_KEY_FILE_PATH: string;
declare const EMBED_GPG_PASSPHRASE: string;
declare const EMBED_GIT_USER_NAME: string;
declare const EMBED_GIT_GPG_KEY_EMAIL: string;
