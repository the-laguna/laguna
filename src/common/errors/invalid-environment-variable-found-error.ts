export default class InvalidEnvironmentVariableFoundError extends Error {
  public constructor(
    public override readonly message: string,
  ) {
    super();
  }
}
