import { EOL } from 'node:os';
import { URL } from 'node:url';
import { format } from 'node:util';
import got from 'got';


interface ProjectGitInfo {
  readonly defaultBranch: string;
  readonly sshUrlToRepo: string;
  readonly httpUrlToRepo: string;
  readonly webUrl: string;
  readonly nameWithNamespace: string;
}

interface GitLabGetProjectRawResponse {
  readonly default_branch: string;
  readonly ssh_url_to_repo: string;
  readonly http_url_to_repo: string;
  readonly web_url: string;
  readonly name_with_namespace: string;
}

export interface GetProjectByIdRequest {
  readonly projectId: number;
  readonly authToken: string;
  readonly gitlabApiBaseUrl: string;
}

export default async function getProjectById(
  request: GetProjectByIdRequest,
): Promise<ProjectGitInfo> {
  const url = new URL(`${request.gitlabApiBaseUrl}/projects/${request.projectId.toString()}`);

  console.debug(
    format(
      [
        '> requesting for project info:',
        '  > url: %s',
        '  > projectId: %s',
      ].join(EOL),
      url.toString(),
      request.projectId,
    ),
  );

  const response = await got
    .get(url.toString(), {
      headers: {
        Authorization: `Bearer ${request.authToken}`,
      },
    })
    .json<GitLabGetProjectRawResponse>();


  return {
    defaultBranch: response.default_branch,
    sshUrlToRepo: response.ssh_url_to_repo,
    httpUrlToRepo: response.http_url_to_repo,
    webUrl: response.web_url,
    nameWithNamespace: response.name_with_namespace,
  };
}
