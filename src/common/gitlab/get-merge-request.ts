import got from 'got';
import type { Response } from 'got';
import { NOT_FOUND, UNAUTHORIZED } from 'http-status';
import NotFoundError from '@common/errors/not-found-error';
import UnauthorizedError from '@common/errors/unauthorized-error';
import UnknownGitlabError from '@common/errors/unknown-gitlab-error';
import type NotFoundResponseBody from './not-found-response-body';


export interface GitLabGetSingleMergeRequestResponse {
  readonly id: number;
  readonly description: string;
  readonly title: string;
}

export interface GitLabGetMergeRequestRequest {
  readonly authToken: string;
  readonly apiBaseUrl: string;
  readonly projectId: number;
  readonly mergeRequestIid: number;
}

export default async function getMergeRequest(
  context: GitLabGetMergeRequestRequest,
): Promise<GitLabGetSingleMergeRequestResponse> {
  try {
    const url = `${context.apiBaseUrl}/projects/${context.projectId.toString(10)}/merge_requests/${context.mergeRequestIid.toString(10)}`;
    return await got
      .get(url, {
        headers: {
          Authorization: `Bearer ${context.authToken}`,
        },
      })
      .json<GitLabGetSingleMergeRequestResponse>();
  } catch (error: unknown) {
    if (error instanceof got.HTTPError) {
      if (error.response.statusCode === UNAUTHORIZED) {
        throw new UnauthorizedError();
      }

      if (error.response.statusCode === NOT_FOUND) {
        throw new NotFoundError(
          (error.response as Response<NotFoundResponseBody>).body.message
          ?? 'Project or merge request not found',
        );
      }

      throw new UnknownGitlabError(
        error.response.statusMessage
        ?? (error.response.body as string | undefined)
        ?? 'unknown error occurred',
      );
    }

    throw error;
  }
}
