export default class NotEnoughReplacements {
  public constructor(
    public readonly exceedingSpecifiers: number,
  ) {}
}
