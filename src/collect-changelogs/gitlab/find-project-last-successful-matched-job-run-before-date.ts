import { URL } from 'node:url';
import dayjs from 'dayjs';
import got from 'got';


export interface FindLastSuccessfulJobRunByNameBeforeDateRequest {
  readonly jobName: string;
  readonly jobStage: string;
  readonly branchName: string;
  readonly maxSearchDate: Date;
  readonly gitlabApiBaseUrl: string;
  readonly authToken: string;
  readonly projectId: number;
}

interface GetProjectJobsRequest {
  readonly gitlabApiBaseUrl: string;
  readonly authToken: string;
  readonly projectId: number;
}

interface GitLabGetProjectJobsRawResponseItem {
  readonly 'status': 'success' | 'skipped' | 'failed' | 'canceled' | 'pending';
  readonly 'stage': string;
  readonly 'name': string;
  readonly 'ref': string;
  readonly 'created_at': string;
  readonly 'started_at': string;
  readonly 'finished_at': string;
}

interface ProjectJob {
  readonly status: 'success' | 'skipped' | 'failed' | 'canceled' | 'pending';
  readonly stage: string;
  readonly name: string;
  readonly ref: string;
  readonly createdAt: Date;
  readonly startedAt: Date;
  readonly finishedAt: Date;
}

async function * generateProjectJobsAfterDate(
  r: GetProjectJobsRequest,
  maxSearchDate: Date,
): AsyncGenerator<readonly ProjectJob[], void> {
  try {
    let page = 1;

    while (true) {
      const url = new URL(`${r.gitlabApiBaseUrl}/projects/${r.projectId.toString()}/jobs`);
      url.searchParams.append('per_page', '100');
      url.searchParams.append('page', page.toString(10));

      // eslint-disable-next-line no-await-in-loop
      const response = await got
        .get(url, { headers: { Authorization: `Bearer ${r.authToken}` } })
        .json<readonly GitLabGetProjectJobsRawResponseItem[]>();

      if (0 === response.length) {
        return;
      }

      const allJobs = response
        .map(job => ({
          status: job.status,
          stage: job.stage,
          name: job.name,
          ref: job.ref,
          createdAt: new Date(job.created_at),
          startedAt: new Date(job.started_at),
          finishedAt: new Date(job.finished_at),
        }));

      const jobsBeforeDate: readonly ProjectJob[] = allJobs
        .filter(job => dayjs(job.createdAt).isAfter(maxSearchDate));

      yield jobsBeforeDate;

      if (allJobs.length !== jobsBeforeDate.length) {
        return;
      }

      page += 1;
    }
  } catch {
    console.debug('handling error...');
  }
}

export interface ProjectJobsFilters {
  readonly maxSearchDate: Date;
  readonly stage: string;
  readonly name: string;
  readonly ref: string;
}

async function generateProjectFirstSuccessfulMatchJob(
  r: GetProjectJobsRequest,
  filters: ProjectJobsFilters,
): Promise<ProjectJob | null> {
  const generator = generateProjectJobsAfterDate(r, filters.maxSearchDate);
  for await (const jobs of generator) {
    const matchedJob = jobs.find(
      job => 'success' === job.status
        && filters.stage === job.stage
        && filters.ref === job.ref
        && filters.name === job.name,
    );

    if (undefined !== matchedJob) {
      await generator.return();
      return matchedJob;
    }
  }

  return null;
}


export default async function findProjectLastSuccessfulMatchedJobRunBeforeDate(
  request: FindLastSuccessfulJobRunByNameBeforeDateRequest,
): Promise<null | Date> {
  const maybeJob = await generateProjectFirstSuccessfulMatchJob(
    {
      authToken: request.authToken,
      gitlabApiBaseUrl: request.gitlabApiBaseUrl,
      projectId: request.projectId,
    },
    {
      name: request.jobName,
      ref: request.branchName,
      stage: request.jobStage,
      maxSearchDate: request.maxSearchDate,
    },
  );

  return maybeJob?.createdAt ?? null;
}
