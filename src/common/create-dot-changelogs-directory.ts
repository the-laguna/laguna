import fs from 'node:fs';
import { format } from 'node:util';
import { KNOWN_ERROR_EXIT_CODE } from './constants';
import HandledError from './errors/handled-error';


export function resolveDotChangelogsDirectoryPath(): string {
  return '.changelogs';
}

export default function createDotChangelogsDirectory(): string {
  const dotChangelogDirectoryPath = resolveDotChangelogsDirectoryPath();

  try {
    // eslint-disable-next-line security/detect-non-literal-fs-filename
    fs.mkdirSync(dotChangelogDirectoryPath, { recursive: true });
  } catch (error: unknown) {
    if (error instanceof Error) {
      throw new HandledError(
        format('unable to create changelog directory: %s', error.message),
        KNOWN_ERROR_EXIT_CODE,
      );
    }

    throw error;
  }

  return dotChangelogDirectoryPath;
}
