import { chmodSync } from 'node:fs';
import type { Plugin } from 'rollup';


export default function executable(): Plugin {
  return {
    name: 'executable',
    writeBundle: options => {
      chmodSync(options.file ?? '', '0755');
    },
  };
}
