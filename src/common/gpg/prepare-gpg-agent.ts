import fs from 'node:fs';
import { EOL } from 'node:os';
import { format } from 'node:util';
import addAuthToGitHttpUrl from '@common/add-auth-to-git-http-url';
import { KNOWN_ERROR_EXIT_CODE } from '@common/constants';
import HandledError from '@common/errors/handled-error';
import { createDefaultExec } from '@common/exec';
import tryGpgSign from './try-gpg-sign';


export default function prepareGpgAgent(
  gitReferenceName: string,
  gitLabRepositoryUrl: string,
  authToken: string,
): void {
  const exec = createDefaultExec();

  try {
    fs.writeFileSync(
      '/root/.gnupg/gpg-agent.conf',
      `default-cache-ttl 720000${EOL}max-cache-ttl 31536000${EOL}allow-preset-passphrase`,
      { encoding: 'utf8' },
    );
  } catch (error: unknown) {
    if (error instanceof AggregateError || error instanceof Error) {
      throw new HandledError(
        format('unable to write to gpg config file: %s', error.message),
        KNOWN_ERROR_EXIT_CODE,
      );
    }

    throw error;
  }

  exec(
    'gpg-connect-agent',
    ['--quiet', '/bye'],
    [],
  );
  exec(
    'gpg-connect-agent',
    ['--quiet', 'RELOADAGENT', '/bye'],
    [],
  );
  exec(
    'gpg',
    [
      '--import',
      '--quiet',
      '--batch',
      '--yes',
      '%s',
    ],
    [EMBED_GPG_KEY_FILE_PATH],
  );
  const lines = exec(
    'gpg',
    [
      '--quiet',
      '--list-secret-keys',
      '--keyid-format',
      'short',
      '--with-colons',
      '%s',
    ],
    [EMBED_GIT_GPG_KEY_EMAIL],
  )
    .stdout
    .trim()
    .split(EOL);
  const fingerprint = lines.find(line => line.startsWith('fpr:'))?.slice('fpr:'.length).replace('::::::::', '').replace(/:$/, '');
  if (undefined === fingerprint) {
    throw new HandledError(
      'unable to extract gpg private key fingerprint',
      KNOWN_ERROR_EXIT_CODE,
    );
  }

  exec(
    'expect',
    [
      '-c',
      '\'spawn gpg --edit-key %s trust quit; send "5\\ry\\r"; expect eof\'',
    ],
    [fingerprint],
  );
  exec(
    'gpg-connect-agent',
    ['RELOADAGENT', '/bye'],
    [],
  );

  const keyGrips = exec(
    'gpg',
    [
      '--quiet',
      '--batch',
      '--with-colons',
      '--with-keygrip',
      '--list-secret-keys',
      '%s',
    ],
    [fingerprint],
  )
    .stdout
    .trim()
    .split(EOL)
    .filter(line => line.startsWith('grp:'))
    .map(line => line.slice('grp'.length).replaceAll(':', ''));
  for (const keyGrip of keyGrips) {
    const hexPassphrase = Buffer.from(EMBED_GPG_PASSPHRASE, 'utf8').toString('hex').toUpperCase();
    exec(
      'gpg-connect-agent',
      ['PRESET_PASSPHRASE %s -1 %s', '/bye'],
      [keyGrip, hexPassphrase],
    );
  }

  tryGpgSign(fingerprint);

  exec(
    'git',
    ['config', 'user.email', '%s'],
    [EMBED_GIT_GPG_KEY_EMAIL],
  );
  exec(
    'git',
    ['config', 'user.name', '%s'],
    [EMBED_GIT_USER_NAME],
  );
  exec(
    'git',
    ['config', 'user.signingkey', '%s'],
    [fingerprint],
  );
  exec(
    'git',
    ['config', 'commit.gpgsign', 'true'],
    [],
  );
  exec(
    'git',
    ['config', 'tag.gpgsign', 'true'],
    [],
  );
  exec(
    'git',
    ['config', 'push.gpgsign', 'if-asked'],
    [],
  );
  exec(
    'git',
    ['config', 'pull.rebase', 'true'],
    [],
  );

  const remoteUrl = addAuthToGitHttpUrl({
    gitUsername: EMBED_GIT_USER_NAME,
    repositoryHttpUrl: gitLabRepositoryUrl,
    authToken,
  });

  exec(
    'git',
    ['remote', 'set-url', 'origin', '%s'],
    [remoteUrl],
  );
  exec(
    'git',
    ['fetch', '--quiet', '-pvPu', '--all', '--auto-gc'],
    [],
  );
  exec(
    'git',
    ['checkout', '--quiet', gitReferenceName],
    [],
  );
  exec(
    'git',
    ['pull', '--quiet', 'origin', gitReferenceName],
    [],
  );
  exec(
    'git',
    ['restore', '--quiet', '--worktree', '--staged', '.'],
    [],
  );
  exec(
    'git',
    ['reset', '--quiet', '--hard'],
    [],
  );
}
