import { ensureExists, ensureIsNotEmpty, ensureIsString, ensureToBeNumberParsable } from './environment-variables';


export default function ensureMergeRequestIidEnvironmentVariable(): void {
  ensureExists('CI_MERGE_REQUEST_IID');

  const value = process.env['CI_MERGE_REQUEST_IID'] ?? '';

  ensureIsNotEmpty('CI_MERGE_REQUEST_IID', value);
  ensureIsString('CI_MERGE_REQUEST_IID', value);
  ensureToBeNumberParsable('CI_MERGE_REQUEST_IID', value);
}
