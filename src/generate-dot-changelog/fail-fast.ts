import { format } from 'node:util';
import { KNOWN_ERROR_EXIT_CODE, UNKNOWN_ERROR_EXIT_CODE } from '@common/constants';
import FailFastError from '@common/errors/fail-fast-error';
import HandledError from '@common/errors/handled-error';
import ensureApiBaseUrlEnvironmentVariable from '@common/fail-fast/ensure-api-base-url-environment-variable';
import ensureAuthTokenEnvironmentVariable from '@common/fail-fast/ensure-auth-token-environment-variable';
import ensureCommitReferenceNameEnvironmentVariable from '@common/fail-fast/ensure-commit-reference-name-environment-variable';
import ensureExpectCommand from '@common/fail-fast/ensure-expect-command';
import ensureGitCommand from '@common/fail-fast/ensure-git-command';
import ensureGitEnvironmentVariables from '@common/fail-fast/ensure-git-environment-variables';
import ensureGPGCommand from '@common/fail-fast/ensure-gpg-command';
import ensureGPGKeyFile from '@common/fail-fast/ensure-gpg-key-file';
import ensureMergeRequestIidEnvironmentVariable from '@common/fail-fast/ensure-merge-request-iid-environment-variable';
import ensureProjectIdEnvironmentVariable from '@common/fail-fast/ensure-project-id-environment-variable';
import ensureRepositoryUrlEnvironmentVariable from '@common/fail-fast/ensure-repository-url-environment-variable';


export default function failFast(): void {
  try {
    ensureApiBaseUrlEnvironmentVariable();
    ensureRepositoryUrlEnvironmentVariable();
    ensureCommitReferenceNameEnvironmentVariable();
    ensureAuthTokenEnvironmentVariable('LAGUNA_GENERATE_TOKEN');
    ensureMergeRequestIidEnvironmentVariable();
    ensureProjectIdEnvironmentVariable();
    ensureGitEnvironmentVariables();
    ensureGPGKeyFile();
    ensureExpectCommand();
    ensureGitCommand();
    ensureGPGCommand();
  } catch (error: unknown) {
    if (error instanceof FailFastError) {
      throw new HandledError(error.message, KNOWN_ERROR_EXIT_CODE);
    }

    throw new HandledError(
      format('unknown error occurred: %s', error),
      UNKNOWN_ERROR_EXIT_CODE,
    );
  }
}
