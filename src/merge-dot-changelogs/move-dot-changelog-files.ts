import { posix as path } from 'node:path';
import buildDotChangelogFilename from '@common/build-dot-changelog-filename';
import { KNOWN_ERROR_EXIT_CODE } from '@common/constants';
import HandledError from '@common/errors/handled-error';
import { createDefaultExec } from '@common/exec';
import resolveDotChangelogFilePath from '@common/resolve-dot-changelog-file-path';
import type { MergeRequestWithMergedDate } from './gitlab/get-merge-request-merged-dates';
import resolveChangelogFilePath from './resolve-changelog-file-path';


interface MovedChangelogFile {
  oldPath: string;
  newPath: string;
}

function moveChangelog(oldPath: string, newPath: string): void {
  const exec = createDefaultExec();

  try {
    exec(
      'mkdir',
      [
        '-p',
        path.dirname(newPath),
      ],
      [],
    );
  } catch {
    throw new HandledError(
      'unable to create directory to new changelog location',
      KNOWN_ERROR_EXIT_CODE,
    );
  }

  try {
    exec(
      'mv',
      [
        oldPath,
        newPath,
      ],
      [],
    );
  } catch {
    throw new HandledError(
      'unable to move temporary changelog file to changelogs directory',
      KNOWN_ERROR_EXIT_CODE,
    );
  }
}

export default function moveDotChangelogFiles(
  dotChangelogsDirectoryPath: string,
  changelogsDirectoryPath: string,
  mergeRequests: MergeRequestWithMergedDate[],
): readonly MovedChangelogFile[] {
  return mergeRequests.map(mergeRequest => {
    const temporaryChangelogFilepath = resolveDotChangelogFilePath(
      dotChangelogsDirectoryPath,
      buildDotChangelogFilename(
        mergeRequest.iid,
      ),
    );

    const committedChangelogFilepath = resolveChangelogFilePath(
      changelogsDirectoryPath,
      mergeRequest,
    );

    moveChangelog(temporaryChangelogFilepath, committedChangelogFilepath);

    return {
      oldPath: temporaryChangelogFilepath,
      newPath: committedChangelogFilepath,
    };
  });
}
