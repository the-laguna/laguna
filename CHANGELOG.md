# laguna

## 4.2.8

### Patch Changes

- d4e77f3: Upgrade outdated dependencies

  @types/node (dev)
  16.11.3 => 16.11.4

  eslint (dev)
  8.0.1 => 8.1.0

  ts-node (dev)
  10.3.1 => 10.4.0

## 4.2.7

### Patch Changes

- eacfdc6: Rename collected changelogs destination directory

  New collected changelogs markdown file is stored into `changelog` directory.

## 4.2.6

### Patch Changes

- 2983fc9: Improve collected changelogs file format

  Make merge request title text bold

## 4.2.5

### Patch Changes

- e4ca812: Fix collected changelogs merge request formatting

## 4.2.5

### Patch Changes

- 3feaf75: Upgrade outdated dependencies

## 4.2.3

### Patch Changes

- eea9b24: Add collected changelog merge request link

## 4.2.2

### Patch Changes

- 1846e22: Improve collected changelogs generated changelog file format

  Move project's merge requests to dropdowns

## 4.2.1

### Patch Changes

- 6db6ae4: Update collected changelogs file heading

## 4.2.0

### Minor Changes

- 25c417c: Collect all project changelogs into one Docusarus-compatible markdown file

## 4.1.16

### Patch Changes

- 60bd1e1: Update outdated dependencies

  - `@rollup/plugin-alias`: 3.1.5 => 3.1.8
  - `@rollup/plugin-commonjs`: 21.0.0 => 21.0.1
  - `@rollup/plugin-node-resolve`: 13.0.5 => 13.0.6
  - `@types/node`: 16.10.3 => 16.11.1
  - `@typescript-eslint/eslint-plugin`: 4.33.0 => 5.1.0
  - `@typescript-eslint/parser`: 4.33.0 => 5.1.0
  - `eslint`: 7.32.0 => 8.0.1
  - `eslint-plugin-import`: 2.24.2 => 2.25.2
  - `eslint-plugin-unicorn`: 36.0.0 => 37.0.1
  - `ts-node`: 10.2.1 => 10.3.0
  - `typescript`: 4.4.3 => 4.4.4
  - `nanoid`: 3.1.29 => 3.1.30

## 4.1.16

### Patch Changes

- ecb9122: Fix wrong Rollup configuration which causes `glob` package to be uncoverted by Rollup

## 4.1.14

### Patch Changes

- 77df3aa: Upgrade npm used in CI and Docker image environments to version 8

## 4.1.14

### Patch Changes

- fbc6cfb: Revert back to use provided `tmp` package api for removing temporary directory

## 4.1.12

### Patch Changes

- de6c682: Update outdated dependency

  - eslint-plugin-regexp: 1.4.0 => 1.4.1

## 4.1.12

### Patch Changes

- d8fa01b: Fix removing temporary downloaded project directory error

  Uknown error was occurring while trying to delete temporary directory created for a project. Now removing that directory is done via 'rm' shell command.

## 4.1.10

### Patch Changes

- 4041c21: Upgrade outdated dependencies

  - @types/node: 6.10.2 => 6.10.3
  - @typescript-eslint/eslint-plugin: 4.32.0 => 4.33.0
  - @typescript-eslint/parser: 4.32.0 => 4.33.0
  - eslint-plugin-regexp: 1.3.1 => 1.4.0
  - nanoid: 3.1.28 => 3.1.29

## 4.1.9

### Patch Changes

- 6abf5b0: Upgrade outdated dependencies

  Upgrade outdated eslint-config-xo package.

## 4.1.8

### Patch Changes

- 6e8c380: Upgrade outdated dependencies

  Upgrade outdated @rollup/plugin-commonjs package

## 4.1.7

### Patch Changes

- e519aae: Upgrade dependencies

  Upgrade outdated `rollup` package

## 4.1.6

### Patch Changes

- 5b689e2: Upgrade dependencies

  Upgrade `@types/node` outdated package.

## 4.1.5

### Patch Changes

- 0f7d5c5: Fix bug getting merge request description from GitLab api call response

  There was a bug in merge request changelog generation which used merge request's title instead of its description for the changelog file content.

## 4.1.4

### Patch Changes

- 82495e1: Add merge request title to collect operation generated changelog

## 4.1.3

### Patch Changes

- bfe8a44: Improve collect operation generated changelog and git commit message date formats

## 4.1.2

### Patch Changes

- ba8bb27: Fix printed collect operation successful run git commit ids
- 47509ee: Use human readable time format in collect job generated changelog
- c8c5b11: Add collect operation date range start to generated changelog file
- 1cdb441: Add UTC timezone offset to collect operation git commit message

## 4.1.1

### Patch Changes

- 051a0f5: Add development docker image builds

  Now, each merge request creates a Docker image in the project's registry, tagged with the merge request'd IID. So, for example, merge request 29 will create three Docker images which their tags will be `dev-29`.

## 4.1.0

### Minor Changes

- ad50a4a: Add Date-Range-Start collect job configuration

## 4.0.1

### Patch Changes

- 7f557e6: Add EXTERNAL_PROJECT_IDS variable validation

## 4.0.0

### Major Changes

- 61ab09a: Add Changelog Collector

### Minor Changes

- 220dd5a: Add Collect Changelogs command handler

## 3.0.1

### Patch Changes

- cf40888: Use relative file and directory paths

## 3.0.0

### Major Changes

- 89e3368: Separate Merge and Generate users auth tokens

## 2.0.0

### Major Changes

- c4ec6ee: Separate merge-dot-changelogs and generate-dot-changelog generated executables

### Minor Changes

- 94d22b8: Add merge-dot-changelogs command
