import type { AssertionError } from 'node:assert';
import assert from 'node:assert/strict';
import { format } from 'node:util';
import execa from 'execa';
import { ONE_SECOND_IN_MILLISECONDS } from '@common/constants';
import FailFastError from '@common/errors/fail-fast-error';
import handleUnknownError from './handle-unknown-error';


function handleAssertionError(
  error: AssertionError,
  messageSuffix: string,
): void {
  const MAKE_SURE_MESSAGE = 'Make sure currently installed expect version is supported.';
  switch (error.message) {
    case 'exit-code': {
      throw new FailFastError(
        format(`%s expected exit code to be 0, got ${error.actual as string}. %s`, messageSuffix, MAKE_SURE_MESSAGE),
      );
    }

    case 'stdout': {
      throw new FailFastError(
        format(`%s expected stdout not to be empty, got ${error.actual as string}. %s`, messageSuffix, MAKE_SURE_MESSAGE),
      );
    }

    case 'failed': {
      throw new FailFastError(
        format('%s command execution has been failed. %s', messageSuffix, MAKE_SURE_MESSAGE),
      );
    }

    case 'killed': {
      throw new FailFastError(
        format('%s command execution has been killed. %s', messageSuffix, MAKE_SURE_MESSAGE),
      );
    }

    case 'stderr': {
      throw new FailFastError(
        format(`%s expected stderr to be empty, got ${error.actual as string}. %s`, messageSuffix, MAKE_SURE_MESSAGE),
      );
    }

    case 'timed-out': {
      throw new FailFastError(
        format('%s command execution time out exceeded. %s', messageSuffix, MAKE_SURE_MESSAGE),
      );
    }

    default: {
      throw new FailFastError(
        format('%s unknown error occurred while ensuring gpg accessibility. %s', messageSuffix, MAKE_SURE_MESSAGE),
      );
    }
  }
}

export default function ensureExpectCommand(): void {
  try {
    console.debug('running "expect -v"');
    const result = execa.sync('expect', ['-v'], { timeout: ONE_SECOND_IN_MILLISECONDS });

    assert.strictEqual(result.exitCode, 0, 'exit-code');
    assert.strictEqual(result.stdout.length > 0, true, 'stdout');
    assert.strictEqual(result.failed, false, 'failed');
    assert.strictEqual(result.killed, false, 'killed');
    assert.strictEqual(0 === result.stderr.length, true, 'stderr');
    assert.strictEqual(result.timedOut, false, 'timed-out');
  } catch (error: unknown) {
    const ERROR_PREPEND_MESSAGE = '[expect]:';
    if (error instanceof assert.AssertionError) {
      handleAssertionError(error, ERROR_PREPEND_MESSAGE);
    }

    if (error instanceof Error) {
      handleUnknownError(error, ERROR_PREPEND_MESSAGE);
    }
  }
}
