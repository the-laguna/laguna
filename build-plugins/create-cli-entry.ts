import { EOL } from 'node:os';
import MagicString from 'magic-string';
import type { Plugin } from 'rollup';


export default function createCliEntry(): Plugin {
  return {
    name: 'create-cli-entry',
    renderChunk(code, chunkInfo) {
      if (chunkInfo.isEntry) {
        const magicString = new MagicString(code);
        return {
          // eslint-disable-next-line @typescript-eslint/no-base-to-string
          code: magicString.prepend(`#!/usr/bin/env node${EOL}`).toString(),
          map: magicString.generateMap({ hires: true }),
        };
      }

      return {
        code,
      };
    },
  };
}
