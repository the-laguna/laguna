import { format } from 'node:util';
import color from 'chalk';


export default class HandledError {
  public constructor(
    public readonly message: string,
    public readonly exitCode: number,
  ) {}

  public print(): void {
    console.error(
      format('%s %s', color.bgRed.black(' [ERROR] '), color.red(this.message)),
    );
  }
}
