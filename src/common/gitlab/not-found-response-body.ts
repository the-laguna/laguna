export default interface NotFoundResponseBody {
  readonly message?: string;
}
