export default function isFilenameAValidChangelogFilename(
  filename: string,
): boolean {
  return /^merge-request-\d+\.md$/.test(filename);
}
