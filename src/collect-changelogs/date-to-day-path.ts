import { posix as path } from 'node:path';
import dayjs from 'dayjs';


export default function dateToDayPath(date: Date): string {
  return dayjs(date).format(['YYYY', 'MM', 'DD'].join(path.sep));
}
