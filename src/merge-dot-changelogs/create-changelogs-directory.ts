import fs from 'node:fs';
import { format } from 'node:util';
import { KNOWN_ERROR_EXIT_CODE } from '@common/constants';
import HandledError from '@common/errors/handled-error';


export function resolveChangelogsDirectoryPath(): string {
  return 'changelogs';
}

export default function createChangelogsDirectory(): string {
  const changelogDirectoryPath = resolveChangelogsDirectoryPath();
  try {
    // eslint-disable-next-line security/detect-non-literal-fs-filename
    fs.mkdirSync(changelogDirectoryPath, { recursive: true });
  } catch (error: unknown) {
    if (error instanceof Error) {
      throw new HandledError(
        format('unable to create changelog directory: %s', error.message),
        KNOWN_ERROR_EXIT_CODE,
      );
    }

    throw error;
  }

  return changelogDirectoryPath;
}
