import { format } from 'node:util';
import { KNOWN_ERROR_EXIT_CODE, UNKNOWN_ERROR_EXIT_CODE } from '@common/constants';
import FailFastError from '@common/errors/fail-fast-error';
import HandledError from '@common/errors/handled-error';
import ensureApiBaseUrlEnvironmentVariable from '../common/fail-fast/ensure-api-base-url-environment-variable';
import ensureAuthTokenEnvironmentVariable from '../common/fail-fast/ensure-auth-token-environment-variable';
import ensureCommitReferenceNameEnvironmentVariable from '../common/fail-fast/ensure-commit-reference-name-environment-variable';
import ensureExpectCommand from '../common/fail-fast/ensure-expect-command';
import ensureGitCommand from '../common/fail-fast/ensure-git-command';
import ensureGitEnvironmentVariables from '../common/fail-fast/ensure-git-environment-variables';
import ensureGPGCommand from '../common/fail-fast/ensure-gpg-command';
import ensureGPGKeyFile from '../common/fail-fast/ensure-gpg-key-file';
import ensureProjectIdEnvironmentVariable from '../common/fail-fast/ensure-project-id-environment-variable';
import ensureRepositoryUrlEnvironmentVariable from '../common/fail-fast/ensure-repository-url-environment-variable';
import { ensureExists, ensureIsString, ensureValidUrl } from '../common/fail-fast/environment-variables';


function ensureUserSetRangeStartDate(): void {
  if (undefined === process.env['LAGUNA_COLLECT_DATE_RANGE_START']) {
    return;
  }

  const value = process.env['LAGUNA_COLLECT_DATE_RANGE_START'].trim();

  if (!['week', 'month'].includes(value)) {
    throw new FailFastError(
      format(
        '"LAGUNA_COLLECT_DATE_RANGE_START" can only set to %s. either delete the variable from your configuration, or set its value to one of the valid options.',
        ['"week"', '"month"'].join(', or'),
      ),
    );
  }
}


function ensureExternalProjectIdsEnvironmentVariable(): void {
  ensureExists('EXTERNAL_PROJECT_IDS');

  const value = process.env['EXTERNAL_PROJECT_IDS'] ?? '';

  // eslint-disable-next-line security/detect-unsafe-regex
  if (!/^\s*\d+(?:\s*-\s*\d+)*\s*$/.test(value)) {
    throw new FailFastError(
      '"EXTERNAL_PROJECT_IDS" must contain only hyphen-separated list of numbers. Make sure there are no empty spaces or new lines.',
    );
  }
}

function ensureJobStageEnvironmentVariables(): void {
  ensureExists('CI_JOB_STAGE');
  ensureExists('CI_JOB_NAME');
}

function ensureServerUrlEnvironmentVariable(): void {
  ensureExists('CI_SERVER_URL');

  const value = process.env['CI_SERVER_URL'] ?? '';

  ensureIsString('CI_SERVER_URL', value);
  ensureValidUrl('CI_SERVER_URL', value);
}


export default function failFast(): void {
  try {
    ensureServerUrlEnvironmentVariable();
    ensureUserSetRangeStartDate();
    ensureApiBaseUrlEnvironmentVariable();
    ensureRepositoryUrlEnvironmentVariable();
    ensureExternalProjectIdsEnvironmentVariable();
    ensureJobStageEnvironmentVariables();
    ensureCommitReferenceNameEnvironmentVariable();
    ensureAuthTokenEnvironmentVariable('LAGUNA_COLLECT_TOKEN');
    ensureProjectIdEnvironmentVariable();
    ensureGitEnvironmentVariables();
    ensureGPGKeyFile();
    ensureExpectCommand();
    ensureGitCommand();
    ensureGPGCommand();
  } catch (error: unknown) {
    if (error instanceof FailFastError) {
      throw new HandledError(error.message, KNOWN_ERROR_EXIT_CODE);
    }

    throw new HandledError(
      format('unknown error occurred: %s', error),
      UNKNOWN_ERROR_EXIT_CODE,
    );
  }
}
