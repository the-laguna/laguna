image: node:16-alpine

workflow:
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_ORIGIN_BRANCH_NAME
    - if: $CI_MERGE_REQUEST_IID

variables:
  CI_ORIGIN_BRANCH_NAME: main

cache: &global_cache
  untracked: true
  key:
    files:
      - pnpm-lock.yaml
      - package.json
  paths:
    - .pnpm-store/
    - node_modules/

default:
  interruptible: true
  tags:
    - specific
  before_script:
    - apk update
    - apk upgrade
    - npm --global install npm@8
    - npm --global install pnpm@6
    - pnpm install --store-dir .pnpm-store --prefer-offline

stages:
  - Check
  - Build
  - Build And Publish Docker Images

Lint:
  stage: Check
  allow_failure: false
  needs: []
  rules:
    - if: $CI_MERGE_REQUEST_IID || $CI_COMMIT_BRANCH
  script:
    - pnpm run lint

Test:
  stage: Check
  allow_failure: false
  needs: []
  rules:
    - if: $CI_MERGE_REQUEST_IID || $CI_COMMIT_BRANCH
  script:
    - pnpm run test

Check Types:
  stage: Check
  allow_failure: false
  needs: []
  rules:
    - if: $CI_MERGE_REQUEST_IID || $CI_COMMIT_BRANCH
  script:
    - pnpm run compile

Build:
  stage: Build
  allow_failure: false
  needs:
    - job: Lint
      artifacts: false
      optional: false
    - job: Test
      artifacts: false
      optional: false
    - job: Check Types
      artifacts: false
      optional: false
  variables:
    EMBED_MERGE_GPG_KEY_FILE_PATH: ""
    EMBED_MERGE_GPG_PASSPHRASE: ""
    EMBED_MERGE_GIT_USER_NAME: ""
    EMBED_MERGE_GIT_GPG_KEY_EMAIL: ""
    EMBED_GENERATE_GPG_KEY_FILE_PATH: ""
    EMBED_GENERATE_GPG_PASSPHRASE: ""
    EMBED_GENERATE_GIT_USER_NAME: ""
    EMBED_GENERATE_GIT_GPG_KEY_EMAIL: ""
    EMBED_COLLECT_GPG_KEY_FILE_PATH: ""
    EMBED_COLLECT_GPG_PASSPHRASE: ""
    EMBED_COLLECT_GIT_USER_NAME: ""
    EMBED_COLLECT_GIT_GPG_KEY_EMAIL: ""
  rules:
    - if: $CI_MERGE_REQUEST_IID || $CI_COMMIT_BRANCH
  script:
    - apk add make
    - make build

.build_docker_image:
  stage: Build And Publish Docker Images
  allow_failure: false
  tags:
    - specific
    - dind
  services:
    - docker:dind
  interruptible: false
  image: docker:latest
  cache: {}
  needs:
    - job: Build
      artifacts: false
      optional: false
  variables:
    IMAGE_TAG: "v4"
  script:
    - echo "$CI_REGISTRY_PASSWORD" | docker login --username "$CI_REGISTRY_USER" --password-stdin "$CI_REGISTRY"
    - printf "%s" "$GPG_KEY_FILE_PATH" > "${CI_PROJECT_DIR}/gpgkeypath"
    - export GPG_KEY_FILE=$(eval "printf '%s' "\$$GPG_KEY_FILE"")
    - DOCKER_BUILDKIT=1 docker build --no-cache --secret="id=gpgkeypath,src=${CI_PROJECT_DIR}/gpgkeypath" --secret="id=dotenv,src=${CI_PROJECT_DIR}/.env" --secret="id=gpgkey,src=$GPG_KEY_FILE" --pull --file "${CI_PROJECT_DIR}/${DOCKERFILE}" --tag "${IMAGE_NAME}:${IMAGE_TAG}" --label maintainer='Morteza Behboodian < hello [ at ] xeptore [ dot ] me >' .
    - docker image push --quiet "${IMAGE_NAME}:${IMAGE_TAG}"

.build_release_docker_image:
  extends: .build_docker_image
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_ORIGIN_BRANCH_NAME
      when: on_success
    - if: $CI_MERGE_REQUEST_IID
      when: never
  after_script:
    - docker image tag "${IMAGE_NAME}:${IMAGE_TAG}" "${IMAGE_NAME}:latest"
    - docker image push --quiet "${IMAGE_NAME}:latest"

.build_development_docker_image:
  extends: .build_docker_image
  rules:
    - if: $CI_MERGE_REQUEST_IID
      when: on_success
    - if: $CI_COMMIT_BRANCH == $CI_ORIGIN_BRANCH_NAME
      when: never
  variables:
    IMAGE_TAG: "dev-$CI_MERGE_REQUEST_IID"

Build And Publish Development Merge Docker Image:
  extends: .build_development_docker_image
  variables:
    IMAGE_NAME: "${CI_REGISTRY_IMAGE}/merge"
    DOCKERFILE: merge.Dockerfile
    GPG_KEY_FILE_PATH: $EMBED_DEV_GPG_KEY_FILE_PATH
    GPG_KEY_FILE: EMBED_DEV_GPG_KEY_FILE
    EMBED_MERGE_GPG_KEY_FILE_PATH: $EMBED_DEV_GPG_KEY_FILE_PATH
    EMBED_MERGE_GPG_PASSPHRASE: $EMBED_DEV_GPG_PASSPHRASE
    EMBED_MERGE_GIT_USER_NAME: $EMBED_DEV_GIT_USER_NAME
    EMBED_MERGE_GIT_GPG_KEY_EMAIL: $EMBED_DEV_GIT_GPG_KEY_EMAIL
  before_script:
    - echo "EMBED_MERGE_GPG_KEY_FILE_PATH='$EMBED_MERGE_GPG_KEY_FILE_PATH'" >> "$CI_PROJECT_DIR"/.env
    - echo "EMBED_MERGE_GPG_PASSPHRASE='$EMBED_MERGE_GPG_PASSPHRASE'" >> "$CI_PROJECT_DIR"/.env
    - echo "EMBED_MERGE_GIT_USER_NAME='$EMBED_MERGE_GIT_USER_NAME'" >> "$CI_PROJECT_DIR"/.env
    - echo "EMBED_MERGE_GIT_GPG_KEY_EMAIL='$EMBED_MERGE_GIT_GPG_KEY_EMAIL'" >> "$CI_PROJECT_DIR"/.env

Build And Publish Development Generate Docker Image:
  extends: .build_development_docker_image
  variables:
    IMAGE_NAME: "${CI_REGISTRY_IMAGE}/generate"
    DOCKERFILE: generate.Dockerfile
    GPG_KEY_FILE_PATH: $EMBED_DEV_GPG_KEY_FILE_PATH
    GPG_KEY_FILE: EMBED_DEV_GPG_KEY_FILE
    EMBED_GENERATE_GPG_KEY_FILE_PATH: $EMBED_DEV_GPG_KEY_FILE_PATH
    EMBED_GENERATE_GPG_PASSPHRASE: $EMBED_DEV_GPG_PASSPHRASE
    EMBED_GENERATE_GIT_USER_NAME: $EMBED_DEV_GIT_USER_NAME
    EMBED_GENERATE_GIT_GPG_KEY_EMAIL: $EMBED_DEV_GIT_GPG_KEY_EMAIL
  before_script:
    - echo "EMBED_GENERATE_GPG_KEY_FILE_PATH='$EMBED_GENERATE_GPG_KEY_FILE_PATH'" >> "$CI_PROJECT_DIR"/.env
    - echo "EMBED_GENERATE_GPG_PASSPHRASE='$EMBED_GENERATE_GPG_PASSPHRASE'" >> "$CI_PROJECT_DIR"/.env
    - echo "EMBED_GENERATE_GIT_USER_NAME='$EMBED_GENERATE_GIT_USER_NAME'" >> "$CI_PROJECT_DIR"/.env
    - echo "EMBED_GENERATE_GIT_GPG_KEY_EMAIL='$EMBED_GENERATE_GIT_GPG_KEY_EMAIL'" >> "$CI_PROJECT_DIR"/.env

Build And Publish Development Collect Docker Image:
  extends: .build_development_docker_image
  variables:
    IMAGE_NAME: "${CI_REGISTRY_IMAGE}/collect"
    DOCKERFILE: collect.Dockerfile
    GPG_KEY_FILE_PATH: $EMBED_DEV_GPG_KEY_FILE_PATH
    GPG_KEY_FILE: EMBED_DEV_GPG_KEY_FILE
    EMBED_COLLECT_GPG_KEY_FILE_PATH: $EMBED_DEV_GPG_KEY_FILE_PATH
    EMBED_COLLECT_GPG_PASSPHRASE: $EMBED_DEV_GPG_PASSPHRASE
    EMBED_COLLECT_GIT_USER_NAME: $EMBED_DEV_GIT_USER_NAME
    EMBED_COLLECT_GIT_GPG_KEY_EMAIL: $EMBED_DEV_GIT_GPG_KEY_EMAIL
  before_script:
    - echo "EMBED_COLLECT_GPG_KEY_FILE_PATH='$EMBED_COLLECT_GPG_KEY_FILE_PATH'" >> "$CI_PROJECT_DIR"/.env
    - echo "EMBED_COLLECT_GPG_PASSPHRASE='$EMBED_COLLECT_GPG_PASSPHRASE'" >> "$CI_PROJECT_DIR"/.env
    - echo "EMBED_COLLECT_GIT_USER_NAME='$EMBED_COLLECT_GIT_USER_NAME'" >> "$CI_PROJECT_DIR"/.env
    - echo "EMBED_COLLECT_GIT_GPG_KEY_EMAIL='$EMBED_COLLECT_GIT_GPG_KEY_EMAIL'" >> "$CI_PROJECT_DIR"/.env

Build And Publish Merge Docker Image:
  extends: .build_release_docker_image
  variables:
    IMAGE_NAME: "${CI_REGISTRY_IMAGE}/merge"
    DOCKERFILE: merge.Dockerfile
    GPG_KEY_FILE_PATH: $EMBED_MERGE_GPG_KEY_FILE_PATH
    GPG_KEY_FILE: EMBED_MERGE_GPG_KEY_FILE
    EMBED_MERGE_GPG_KEY_FILE_PATH: $EMBED_MERGE_GPG_KEY_FILE_PATH
    EMBED_MERGE_GPG_PASSPHRASE: $EMBED_MERGE_GPG_PASSPHRASE
    EMBED_MERGE_GIT_USER_NAME: $EMBED_MERGE_GIT_USER_NAME
    EMBED_MERGE_GIT_GPG_KEY_EMAIL: $EMBED_MERGE_GIT_GPG_KEY_EMAIL
  before_script:
    - echo "EMBED_MERGE_GPG_KEY_FILE_PATH='$EMBED_MERGE_GPG_KEY_FILE_PATH'" >> "$CI_PROJECT_DIR"/.env
    - echo "EMBED_MERGE_GPG_PASSPHRASE='$EMBED_MERGE_GPG_PASSPHRASE'" >> "$CI_PROJECT_DIR"/.env
    - echo "EMBED_MERGE_GIT_USER_NAME='$EMBED_MERGE_GIT_USER_NAME'" >> "$CI_PROJECT_DIR"/.env
    - echo "EMBED_MERGE_GIT_GPG_KEY_EMAIL='$EMBED_MERGE_GIT_GPG_KEY_EMAIL'" >> "$CI_PROJECT_DIR"/.env

Build And Publish Generate Docker Image:
  extends: .build_release_docker_image
  variables:
    IMAGE_NAME: "${CI_REGISTRY_IMAGE}/generate"
    DOCKERFILE: generate.Dockerfile
    GPG_KEY_FILE_PATH: $EMBED_GENERATE_GPG_KEY_FILE_PATH
    GPG_KEY_FILE: EMBED_GENERATE_GPG_KEY_FILE
    EMBED_GENERATE_GPG_KEY_FILE_PATH: $EMBED_GENERATE_GPG_KEY_FILE_PATH
    EMBED_GENERATE_GPG_PASSPHRASE: $EMBED_GENERATE_GPG_PASSPHRASE
    EMBED_GENERATE_GIT_USER_NAME: $EMBED_GENERATE_GIT_USER_NAME
    EMBED_GENERATE_GIT_GPG_KEY_EMAIL: $EMBED_GENERATE_GIT_GPG_KEY_EMAIL
  before_script:
    - echo "EMBED_GENERATE_GPG_KEY_FILE_PATH='$EMBED_GENERATE_GPG_KEY_FILE_PATH'" >> "$CI_PROJECT_DIR"/.env
    - echo "EMBED_GENERATE_GPG_PASSPHRASE='$EMBED_GENERATE_GPG_PASSPHRASE'" >> "$CI_PROJECT_DIR"/.env
    - echo "EMBED_GENERATE_GIT_USER_NAME='$EMBED_GENERATE_GIT_USER_NAME'" >> "$CI_PROJECT_DIR"/.env
    - echo "EMBED_GENERATE_GIT_GPG_KEY_EMAIL='$EMBED_GENERATE_GIT_GPG_KEY_EMAIL'" >> "$CI_PROJECT_DIR"/.env

Build And Publish Collect Docker Image:
  extends: .build_release_docker_image
  variables:
    IMAGE_NAME: "${CI_REGISTRY_IMAGE}/collect"
    DOCKERFILE: collect.Dockerfile
    GPG_KEY_FILE_PATH: $EMBED_COLLECT_GPG_KEY_FILE_PATH
    GPG_KEY_FILE: EMBED_COLLECT_GPG_KEY_FILE
    EMBED_COLLECT_GPG_KEY_FILE_PATH: $EMBED_COLLECT_GPG_KEY_FILE_PATH
    EMBED_COLLECT_GPG_PASSPHRASE: $EMBED_COLLECT_GPG_PASSPHRASE
    EMBED_COLLECT_GIT_USER_NAME: $EMBED_COLLECT_GIT_USER_NAME
    EMBED_COLLECT_GIT_GPG_KEY_EMAIL: $EMBED_COLLECT_GIT_GPG_KEY_EMAIL
  before_script:
    - echo "EMBED_COLLECT_GPG_KEY_FILE_PATH='$EMBED_COLLECT_GPG_KEY_FILE_PATH'" >> "$CI_PROJECT_DIR"/.env
    - echo "EMBED_COLLECT_GPG_PASSPHRASE='$EMBED_COLLECT_GPG_PASSPHRASE'" >> "$CI_PROJECT_DIR"/.env
    - echo "EMBED_COLLECT_GIT_USER_NAME='$EMBED_COLLECT_GIT_USER_NAME'" >> "$CI_PROJECT_DIR"/.env
    - echo "EMBED_COLLECT_GIT_GPG_KEY_EMAIL='$EMBED_COLLECT_GIT_GPG_KEY_EMAIL'" >> "$CI_PROJECT_DIR"/.env
