const HOURS_IN_A_DAY = 24;
const MINUTES_IN_AN_HOUR = 60;
const SECONDS_IN_A_MINUTE = 60;
const MILLISECONDS_IN_A_SECOND = 1000;
const DAY_IN_MINUTES = HOURS_IN_A_DAY * MINUTES_IN_AN_HOUR;
const DAY_IN_SECONDS = DAY_IN_MINUTES * SECONDS_IN_A_MINUTE;
const DAY_IN_MILLISECONDS = DAY_IN_SECONDS * MILLISECONDS_IN_A_SECOND;


export default function daysInRange(
  startDate: Date,
  endDate: Date,
): readonly Date[] {
  const endDateTime = endDate.getTime();
  const startDateTime = startDate.getTime();
  const diffInMilliseconds = endDateTime - startDateTime;
  const diffInDays = diffInMilliseconds / DAY_IN_MILLISECONDS;

  const hasMoreThanHalfOfADay = Math.round(diffInDays) > Math.floor(diffInDays);

  const numberOfFullDays = diffInDays + (hasMoreThanHalfOfADay ? 1 : 0);

  return Array
    .from(
      { length: numberOfFullDays },
      (_, index) => new Date(DAY_IN_MILLISECONDS * index + startDateTime),
    )
    .concat(new Date(endDate));
}
