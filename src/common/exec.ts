import { format } from 'node:util';
import color from 'chalk';
import execa from 'execa';
import { UNKNOWN_ERROR_EXIT_CODE } from './constants';
import type {
  WithErrnoError,
  WithOptionalExitCodeError,
  WithStderrError,
} from './errors/fail-fast-error';
import HandledError from './errors/handled-error';
import NotEnoughReplacements from './errors/not-enough-replacements';
import NotEnoughSpecifiers from './errors/not-enough-specifiers';


export interface ExecResult {
  readonly stdout: string;
  readonly stderr: string;
  readonly exitCode: number | null;
  readonly failed: boolean;
  readonly killed: boolean;
  readonly timedOut: boolean;
}

export type ExecCommandArgumentsFormatter = (
  commandArguments: string[],
  replacements: string[],
) => string[];

export type Redactor = (
  command: string,
  commandArguments: string[],
  replacements: string[],
) => string;

export interface CreateExecConfig {
  readonly redactor: Redactor;
  readonly execCommandArgumentsFormatter: ExecCommandArgumentsFormatter;
}

export function createDefaultRedactor(): Redactor {
  return (
    command,
    commandArguments,
    replacements,
  ) => {
    const specifiers = Array.from(
      commandArguments.join(' ').matchAll(/%s/g),
      m => m.index,
    );

    if (replacements.length > specifiers.length) {
      throw new NotEnoughSpecifiers(
        replacements.slice(specifiers.length),
      );
    }

    if (specifiers.length > replacements.length) {
      throw new NotEnoughReplacements(
        specifiers.length - replacements.length,
      );
    }

    return format(
      '+ %s %s',
      command,
      format(
        commandArguments.map(ca => `'${ca}'`).join(' '),
        ...replacements.map(r => '*'.repeat(r.length)),
      ),
    );
  };
}

export function
createDefaultExecCommandArgumentsFormatter(): ExecCommandArgumentsFormatter {
  return (
    commandArguments,
    replacements,
  ) => {
    let replacementIndex = 0;

    return commandArguments
      .map(ca => {
        if (ca.includes('%s')) {
          return format(ca, replacements[replacementIndex++]);
        }

        return ca;
      });
  };
}

export function resolveExecaErrorMessage(error: unknown): string {
  if (Object.prototype.hasOwnProperty.call(error, 'errno')) {
    return format(
      'command failed. make sure the command is accessible. ERRNO: %s',
      (error as WithErrnoError).errno,
    );
  }

  return (error as WithStderrError).stderr;
}

export function resolveExecaErrorExitCode(error: unknown): number {
  return (error as WithOptionalExitCodeError)
    .exitCode ?? UNKNOWN_ERROR_EXIT_CODE;
}

export type Exec = (
  command: string,
  commandArguments: string[],
  replacements: string[],
) => ExecResult;

export function createExec(config: CreateExecConfig): Exec {
  return function (
    command,
    commandArguments,
    replacements,
  ): ExecResult {
    const redactedMessage = config.redactor(
      command,
      commandArguments,
      replacements,
    );

    console.debug(color.blue(redactedMessage));
    try {
      const out = execa.sync(
        command,
        config.execCommandArgumentsFormatter(
          commandArguments,
          replacements,
        ),
      );


      return {
        exitCode: out.exitCode,
        stderr: out.stderr,
        stdout: out.stdout,
        failed: out.failed,
        killed: out.killed,
        timedOut: out.timedOut,
      };
    } catch (error: unknown) {
      const message = resolveExecaErrorMessage(error);
      const exitCode = resolveExecaErrorExitCode(error);
      throw new HandledError(message, exitCode);
    }
  };
}

export function createDefaultExec(): Exec {
  return createExec({
    execCommandArgumentsFormatter: createDefaultExecCommandArgumentsFormatter(),
    redactor: createDefaultRedactor(),
  });
}
