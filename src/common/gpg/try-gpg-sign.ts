import fs from 'node:fs';
import { format } from 'node:util';
import { KNOWN_ERROR_EXIT_CODE, UNKNOWN_ERROR_EXIT_CODE } from '@common/constants';
import HandledError from '@common/errors/handled-error';
import { createDefaultExec } from '@common/exec';


export default function tryGpgSign(fingerprint: string): void {
  const exec = createDefaultExec();

  const temporaryFile = exec('mktemp', [], []).stdout.trim();
  const passphraseTemporaryFile = exec('mktemp', [], []).stdout.trim();
  try {
    // eslint-disable-next-line security/detect-non-literal-fs-filename
    fs.writeFileSync(
      passphraseTemporaryFile,
      EMBED_GPG_PASSPHRASE,
      { encoding: 'utf8' },
    );
    // eslint-disable-next-line security/detect-non-literal-fs-filename
    fs.writeFileSync(temporaryFile, 'empty', { encoding: 'utf8' });
  } catch (error: unknown) {
    if (error instanceof AggregateError || error instanceof Error) {
      throw new HandledError(
        format('unable to write to temporary files: %s', error.message),
        KNOWN_ERROR_EXIT_CODE,
      );
    }

    throw error;
  }


  const gpgSignCommand = exec(
    'gpg',
    [
      '--quiet',
      '--sign',
      '--pinentry-mode',
      'loopback',
      '--passphrase-file',
      '%s',
      '--local-user',
      '%s',
      '--output',
      '/dev/null',
      '--yes',
      temporaryFile,
    ],
    [passphraseTemporaryFile, fingerprint],
  );

  try {
    fs.rmSync(passphraseTemporaryFile, { force: true });
    fs.rmSync(temporaryFile, { force: true });
  } catch (error: unknown) {
    if (error instanceof Error) {
      throw new HandledError(
        format('unable to remove temporary files: %s', error.message),
        KNOWN_ERROR_EXIT_CODE,
      );
    }

    throw error;
  }

  if (0 === gpgSignCommand.exitCode) {
    return;
  }

  throw new HandledError(
    format('unable to sign temporary file: %s', gpgSignCommand.stderr),
    gpgSignCommand.exitCode ?? UNKNOWN_ERROR_EXIT_CODE,
  );
}
