import { ensureIsNotEmpty, ensureIsString } from './environment-variables';


export default function ensureGitEnvironmentVariables(): void {
  const gitUserName = EMBED_GIT_USER_NAME;
  const gpgEmail = EMBED_GIT_GPG_KEY_EMAIL;

  ensureIsNotEmpty('GIT_USER_NAME', gitUserName);
  ensureIsNotEmpty('GIT_GPG_KEY_EMAIL', gpgEmail);
  ensureIsString('GIT_USER_NAME', gitUserName);
  ensureIsString('GIT_GPG_KEY_EMAIL', gpgEmail);
}
