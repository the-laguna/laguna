import { ensureExists, ensureIsNotEmpty, ensureIsString, ensureToBeNumberParsable } from './environment-variables';


export default function ensureProjectIdEnvironmentVariable(): void {
  ensureExists('CI_PROJECT_ID');

  const value = process.env['CI_PROJECT_ID'] ?? '';

  ensureIsNotEmpty('CI_PROJECT_ID', value);
  ensureIsString('CI_PROJECT_ID', value);
  ensureToBeNumberParsable('CI_PROJECT_ID', value);
}
