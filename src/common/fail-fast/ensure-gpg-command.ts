import type { AssertionError } from 'node:assert';
import assert from 'node:assert/strict';
import { format } from 'node:util';
import execa from 'execa';
import { ONE_SECOND_IN_MILLISECONDS } from '@common/constants';
import FailFastError from '@common/errors/fail-fast-error';
import handleUnknownError from './handle-unknown-error';


function handleAssertionError(
  error: AssertionError,
  messageSuffix: string,
): void {
  const MAKE_SURE_MESSAGE = 'Make sure currently installed gpg version is supported.';
  switch (error.message) {
    case 'exit-code': {
      throw new FailFastError(
        format(
          `${messageSuffix}expected exit code to be 0, got ${error.actual as string}. %s`,
          MAKE_SURE_MESSAGE,
        ),
      );
    }

    case 'stdout': {
      throw new FailFastError(
        format(`${messageSuffix}expected stdout not to be empty, got ${error.actual as string}. %s`, MAKE_SURE_MESSAGE),
      );
    }

    case 'failed': {
      throw new FailFastError(
        format(`${messageSuffix}command execution has been failed. %s`, MAKE_SURE_MESSAGE),
      );
    }

    case 'killed': {
      throw new FailFastError(
        format(`${messageSuffix}command execution has been killed. %s`, MAKE_SURE_MESSAGE),
      );
    }

    case 'stderr': {
      throw new FailFastError(
        format(`${messageSuffix}expected stderr to be empty, got ${error.actual as string}. %s`, MAKE_SURE_MESSAGE),
      );
    }

    case 'timed-out': {
      throw new FailFastError(
        format(`${messageSuffix}command execution time out exceeded. %s`, MAKE_SURE_MESSAGE),
      );
    }

    default: {
      throw new FailFastError(
        format(`${messageSuffix}unknown error occurred while ensuring gpg accessibility. %s`, MAKE_SURE_MESSAGE),
      );
    }
  }
}

export default function ensureGPGCommand(): void {
  try {
    console.debug('running "gpg --version"');
    const result = execa.sync(
      'gpg',
      ['--version'],
      { timeout: ONE_SECOND_IN_MILLISECONDS },
    );

    assert.strictEqual(result.exitCode, 0, 'exit-code');
    assert.strictEqual(result.stdout.length > 0, true, 'stdout');
    assert.strictEqual(result.failed, false, 'failed');
    assert.strictEqual(result.killed, false, 'killed');
    assert.strictEqual(0 === result.stderr.length, true, 'stderr');
    assert.strictEqual(result.timedOut, false, 'timed-out');
  } catch (error: unknown) {
    const ERROR_PREPEND_MESSAGE = '[gpg]: ';
    if (error instanceof assert.AssertionError) {
      handleAssertionError(error, ERROR_PREPEND_MESSAGE);
    }

    if (error instanceof Error) {
      handleUnknownError(error, ERROR_PREPEND_MESSAGE);
    }
  }
}
