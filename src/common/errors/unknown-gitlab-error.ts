export default class UnknownGitlabError {
  public constructor(
    public readonly message: string,
  ) {}
}
