export default class FailFastError {
  public constructor(
    public readonly message: string,
  ) {}
}

export interface WithOriginalMessageError extends Error {
  readonly originalMessage: string;
}

export interface WithCodeError extends Error {
  readonly code: string;
}

export interface WithSyscallError extends Error {
  readonly syscall: string;
}

export interface WithStdoutError extends Error {
  readonly stdout: string;
}

export interface WithStderrError extends Error {
  readonly stderr: string;
}

export interface WithCommandError extends Error {
  readonly command: string;
}

export interface WithOptionalExitCodeError extends Error {
  readonly exitCode?: undefined | number;
}

export interface WithErrnoError extends Error {
  readonly errno: number;
}
