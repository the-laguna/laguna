export interface Logger {
  readonly log: (message: string) => void;
  readonly debug: (message: string) => void;
}

export function formattedTime(date: Date): string {
  return `[${date.getUTCFullYear().toString(10).padStart(4, '0')}-${(date.getUTCMonth() + 1).toString(10).padStart(2, '0')}-${date.getUTCDate().toString(10).padStart(2, '0')} ${date.getUTCHours().toString(10).padStart(2, '0')}:${date.getUTCMinutes().toString(10).padStart(2, '0')}:${date.getUTCSeconds().toString(10).padStart(2, '0')}.${date.getUTCMilliseconds().toString(10).padStart(3, '0')}]`;
}

export function createLogger(enableDebug: boolean): Logger {
  if (!enableDebug) {
    return {
      // eslint-disable-next-line @typescript-eslint/no-empty-function
      debug: () => {},
      log: message => console.error(`${formattedTime(new Date())} ${message}`),
    };
  }

  return {
    debug: message => console.error(`[DEBUG] ${formattedTime(new Date())} ${message}`),
    log: message => console.error(`${formattedTime(new Date())} ${message}`),
  };
}
