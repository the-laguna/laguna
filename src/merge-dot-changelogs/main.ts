import { EOL } from 'node:os';
import { UNKNOWN_ERROR_EXIT_CODE } from '@common/constants';
import createBaseProgram from '@common/create-base-program';
import { resolveDotChangelogsDirectoryPath } from '@common/create-dot-changelogs-directory';
import HandledError from '@common/errors/handled-error';
import { createDefaultExec } from '@common/exec';
import prepareGpgAgent from '@common/gpg/prepare-gpg-agent';
import anyDotChangelogExists from './any-dot-changelog-exists';
import { resolveChangelogsDirectoryPath } from './create-changelogs-directory';
import failFast from './fail-fast';
import getTemporaryChangelogIds from './get-dot-changelog-merge-request-ids';
import getMergeRequestMergedDates from './gitlab/get-merge-request-merged-dates';
import moveDotChangelogFiles from './move-dot-changelog-files';


async function run(): Promise<void> {
  try {
    failFast();

    if (
      undefined === process.env['CI_COMMIT_REF_NAME']
    || undefined === process.env['CI_API_V4_URL']
    || undefined === process.env['CI_REPOSITORY_URL']
    || undefined === process.env['LAGUNA_MERGE_TOKEN']
    || undefined === process.env['CI_PROJECT_ID']
    ) {
      console.error('although required environment variables are checked. but still there is a missing one. weird! exiting.');
      process.exitCode = 1;
      return;
    }


    if (!anyDotChangelogExists()) {
      console.debug('no changelog file exists. exiting...');
      process.exitCode = 0;
      return;
    }

    prepareGpgAgent(
      process.env['CI_COMMIT_REF_NAME'],
      process.env['CI_REPOSITORY_URL'],
      process.env['LAGUNA_MERGE_TOKEN'],
    );

    const dotChangelogsDirectoryPath = resolveDotChangelogsDirectoryPath();

    const changelogIds = getTemporaryChangelogIds(dotChangelogsDirectoryPath);

    const mergeRequestMergedDates = await getMergeRequestMergedDates(
      {
        apiBaseUrl: process.env['CI_API_V4_URL'],
        authToken: process.env['LAGUNA_MERGE_TOKEN'],
        projectId: process.env['CI_PROJECT_ID'],
      },
      changelogIds,
    );

    const changelogsDirectoryPath = resolveChangelogsDirectoryPath();

    const movedChangelogFiles = moveDotChangelogFiles(
      dotChangelogsDirectoryPath,
      changelogsDirectoryPath,
      mergeRequestMergedDates,
    );

    const exec = createDefaultExec();

    const commitIds = movedChangelogFiles
      .map(movedChangelogFile => {
        exec(
          'git',
          [
            'add',
            movedChangelogFile.oldPath,
            movedChangelogFile.newPath,
          ],
          [],
        );
        exec(
          'git',
          [
            'commit',
            '-m',
            'docs: move temporary changelog file to changelogs directory',
            '-m',
            [
              'temporary changelog file moved from',
              `'${movedChangelogFile.oldPath}'`,
              'to',
              `'${movedChangelogFile.newPath}'`,
              `by @${EMBED_GIT_USER_NAME}`,
            ].join(' '),
          ],
          [],
        );
        const commitId = exec('git', ['rev-parse', 'HEAD'], []).stdout.trim();
        return commitId;
      });

    exec('git', ['push'], []);
    console.debug(commitIds.join(EOL));
  } catch (error: unknown) {
    if (error instanceof HandledError) {
      error.print();
      process.exitCode = error.exitCode;
      return;
    }

    console.error('unknown error: %s', error);
    process.exitCode = UNKNOWN_ERROR_EXIT_CODE;
  }
}

function mergeDotChangelogs(): void {
  const program = createBaseProgram();

  program
    .action(async () => await run());

  program
    .parse(process.argv);
}

mergeDotChangelogs();
