/* eslint-disable node/no-unpublished-import */
/* eslint-disable filenames/match-exported */
import { EOL } from 'node:os';
import type { RollupOptions } from 'rollup';
import packageJSON from './package.json';
import { createSharedConfigs, ensureEnvironmentVariablesSet } from './rollup.config';


function provideReplacements(): Record<string, string> {
  ensureEnvironmentVariablesSet([
    'EMBED_MERGE_GPG_KEY_FILE_PATH',
    'EMBED_MERGE_GPG_PASSPHRASE',
    'EMBED_MERGE_GIT_USER_NAME',
    'EMBED_MERGE_GIT_GPG_KEY_EMAIL',
  ]);

  return {
    // eslint-disable-next-line @typescript-eslint/naming-convention
    EMBED_PACKAGE_NAME: JSON.stringify(
      `${packageJSON.name}-merge-dot-changelogs`,
    ),
    // eslint-disable-next-line @typescript-eslint/naming-convention
    EMBED_PACKAGE_VERSION: JSON.stringify(packageJSON.version),
    // eslint-disable-next-line @typescript-eslint/naming-convention
    EMBED_PACKAGE_DESCRIPTION: JSON.stringify(
      [
        'Persists any temporarily stored changelog files on default branch',
        'by moving them from .changelogs directory to changelogs directory.',
      ].join(EOL),
    ),
    // eslint-disable-next-line @typescript-eslint/naming-convention
    EMBED_GPG_KEY_FILE_PATH: JSON.stringify(
      process.env['EMBED_MERGE_GPG_KEY_FILE_PATH'],
    ),
    // eslint-disable-next-line @typescript-eslint/naming-convention
    EMBED_GPG_PASSPHRASE: JSON.stringify(process.env['EMBED_MERGE_GPG_PASSPHRASE']),
    // eslint-disable-next-line @typescript-eslint/naming-convention
    EMBED_GIT_USER_NAME: JSON.stringify(process.env['EMBED_MERGE_GIT_USER_NAME']),
    // eslint-disable-next-line @typescript-eslint/naming-convention
    EMBED_GIT_GPG_KEY_EMAIL: JSON.stringify(
      process.env['EMBED_MERGE_GIT_GPG_KEY_EMAIL'],
    ),
  };
}


const rollupConfig: RollupOptions = {
  input: './src/merge-dot-changelogs/main',
  output: {
    file: `./bin/${packageJSON.name}-merge-dot-changelogs`,
    format: 'cjs',
  },
  ...createSharedConfigs(
    provideReplacements,
  ),
};

export default rollupConfig;
