export default class NotEnoughSpecifiers {
  public constructor(
    public readonly replacements: readonly string[],
  ) {}
}
