import { format } from 'node:util';
import type {
  WithCodeError,
  WithCommandError,
  WithOriginalMessageError,
  WithStderrError,
  WithStdoutError,
  WithSyscallError,
} from '@common/errors/fail-fast-error';
import FailFastError from '@common/errors/fail-fast-error';


export default function handleUnknownError(
  error: Error,
  messageSuffix: string,
): void {
  if (Object.prototype.hasOwnProperty.call(error, 'command')) {
    throw new FailFastError(
      format('%s command: %s', messageSuffix, (error as WithCommandError).command),
    );
  }

  if (Object.prototype.hasOwnProperty.call(error, 'originalMessage')) {
    throw new FailFastError(
      format('%s message: %s', messageSuffix, (error as WithOriginalMessageError).originalMessage),
    );
  }

  if (Object.prototype.hasOwnProperty.call(error, 'code')) {
    throw new FailFastError(
      format('%s code: %s', messageSuffix, (error as WithCodeError).code),
    );
  }

  if (Object.prototype.hasOwnProperty.call(error, 'syscall')) {
    throw new FailFastError(
      format('%s syscall: %s', messageSuffix, (error as WithSyscallError).syscall),
    );
  }

  if (Object.prototype.hasOwnProperty.call(error, 'stdout')) {
    throw new FailFastError(
      format('%s stdout: %s', messageSuffix, (error as WithStdoutError).stdout.trim()),
    );
  }

  if (Object.prototype.hasOwnProperty.call(error, 'stderr')) {
    throw new FailFastError(
      format('%s stderr: %s', messageSuffix, (error as WithStderrError).stderr),
    );
  }

  if (Object.prototype.hasOwnProperty.call(error, 'timedOut')) {
    throw new FailFastError(
      format('%s timedOut', messageSuffix),
    );
  }
}
