compile:
	pnpm run compile
.PHONY: compile

build-generate: compile
	npm run --prefix build nodify:generate
.PHONY: build-generate

build-merge: compile
	npm run --prefix build nodify:merge
.PHONY: build-merge

build-collect: compile
	npm run --prefix build nodify:collect
.PHONY: build-collect

build: build-generate build-merge build-collect
.PHONY: build