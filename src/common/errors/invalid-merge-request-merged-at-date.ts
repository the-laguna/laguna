export default class InvalidMergeRequestMergedAtDate {
  public constructor(
    public readonly mergedAtDate: string,
    public readonly mergeRequestIid: string,
  ) {}
}
